import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.util.Random;

public class Circle  implements Runnable, ActionListener{

	// wspolny bufor
	protected Graphics2D buffer;
	protected Area area;
	// do wykreslania
	protected Shape shape;
	// przeksztalcenie obiektu
	protected AffineTransform aft;
	Rectangle bounds;
	// przesuniecie
	private int dx, dy;

	private int delay;
	private int width;
	private int height;
	private Color clr;
	
	protected static final Random rand = new Random();
	
	public Circle(Graphics2D buf, int del, int w, int h) {
		delay = del;
		buffer = buf;
		width = w;
		height = h;

		dx = 1 + rand.nextInt(5);
		dy = 1 + rand.nextInt(5);

		clr = new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255), rand.nextInt(255));
		
		shape = new Ellipse2D.Float(0, 0, 50, 50);
		aft = new AffineTransform();                                  
		area = new Area(shape);
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		// wypelnienie obiektu
		buffer.setColor(clr.brighter());
		buffer.fill(shape);
		// wykreslenie ramki
		buffer.setColor(clr.darker());
		buffer.draw(shape);
		
	}
	
	@Override
	public void run() {
		// przesuniecie na srodek
		aft.translate(100, 100);
		area.transform(aft);
		shape = area;

		while (true) {
			// przygotowanie nastepnego kadru
			shape = nextFrame();
			try {
				Thread.sleep(delay);
			} catch (InterruptedException e) {
			}
		}
	}

	protected Shape nextFrame() {
		// zapamietanie na zmiennej tymczasowej
		// aby nie przeszkadzalo w wykreslaniu
		area = new Area(area);
		aft = new AffineTransform();
		// odbicie
		Bounce();
		aft.translate(dx, dy);
		area.transform(aft);
		
		return area;
	}
	
	void ChangeDirection(int direction)
	{
		switch(direction)
		{
		case 1: // do gory
			dx = 0;
			dy = -10;
			break;
		case 2: // do dolu
			dx = 0;
			dy = 10;
			break;
		case 3: // w lewo
			dx = -10;
			dy = 0;
			break; 
		case 4: // w prawo
			dx = 10;
			dy = 0;
			break;
		}
	}
	
	void Bounce()
	{
		bounds = area.getBounds();
		int cx = bounds.x + 65;
		int cy = bounds.y + 85;  
		if (bounds.x < 0 || cx > width)
			dx = -dx;
		if (bounds.y < 0 || cy > height)
			dy = -dy;
	}
	
	boolean isCollide(Rectangle bounds1, Rectangle bounds2)
	{
		if(bounds1.getBounds().contains(bounds2)) // nie testowane, nie starczylo czasu
		{
			return true;
		}
		else
			return false;
	}
}

