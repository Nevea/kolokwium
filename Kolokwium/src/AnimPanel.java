import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.MemoryImageSource;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.Timer;

public class AnimPanel extends JPanel implements ActionListener, KeyListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// bufor
	Image image;
	// wykreslacz ekranowy
	Graphics2D device;
	// wykreslacz bufora
	Graphics2D buffer;
	//private List<Figura> fig = new ArrayList<Figura>();
	private List<Thread> threads = new ArrayList<Thread>();
	private List<Circle> circles = new ArrayList<Circle>();
	
	private int delay = 17;

	private Timer timer;
	
	
	private static int numer = 0;

	public AnimPanel() {
		super();
		setBackground(Color.WHITE);
		timer = new Timer(delay, this);
		int spawn = 5000; //milliseconds
		ActionListener taskPerformer = new ActionListener() {
		      public void actionPerformed(ActionEvent evt) {
		    	  addCircle();
		      }
		  };
		new Timer(spawn, taskPerformer).start();
	}

	public void initialize() {
		int width = getWidth();
		int height = getHeight();

		image = createImage(width, height);
		buffer = (Graphics2D) image.getGraphics();
		buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		device = (Graphics2D) getGraphics();
		device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		addKeyListener(this);
	}

	void addCircle() {
		numer++;
		circles.add(new Circle(buffer, delay, getWidth(), getHeight()));
		timer.addActionListener(circles.get(numer-1));
		threads.add(new Thread(circles.get(numer-1)));
		threads.get(threads.size()-1).start();
	}
	
	@SuppressWarnings("deprecation")
	void animate() {
		if (timer.isRunning()) {
			timer.stop();
		} else {
			timer.start();
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
			device.drawImage(image, 0, 0, null);
			buffer.clearRect(0, 0, getWidth(), getHeight());
			
			for(int i = 0; i < numer; i++)
			{
				for(int j = i+1; j < numer; j++)
				{
					if(circles.get(i).isCollide(circles.get(i).bounds, circles.get(j).bounds))
						circles.get(i).Bounce();
				}
			}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_UP)
		{
			System.out.println("up");
			for(int i = 0; i < numer; i++)
				circles.get(i).ChangeDirection(1);
		}
		if(e.getKeyCode() == KeyEvent.VK_DOWN)
		{
			System.out.println("down");
			for(int i = 0; i < numer; i++)
				circles.get(i).ChangeDirection(2);
		}
		if(e.getKeyCode() == KeyEvent.VK_LEFT)
		{
			System.out.println("left");
			for(int i = 0; i < numer; i++)
				circles.get(i).ChangeDirection(3);
		}
		if(e.getKeyCode() == KeyEvent.VK_RIGHT)
		{
			System.out.println("right");
			for(int i = 0; i < numer; i++)
				circles.get(i).ChangeDirection(4);
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
}